<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/12/19
 * Time: 1:22 PM
 */

namespace LaravelSU\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','su'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}