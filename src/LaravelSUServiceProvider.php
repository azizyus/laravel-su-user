<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/12/19
 * Time: 1:20 PM
 */

namespace LaravelSU;


use Illuminate\Support\ServiceProvider;
use LaravelSU\Console\Commands\CreateExampleRootUser;
use LaravelSU\Console\Commands\CreateRootUser;
use LaravelSU\Console\Commands\PromoteSU;
use LaravelSU\Console\Commands\RemoveRootUser;
use LaravelSU\Console\Commands\RevokeSU;

class LaravelSUServiceProvider extends ServiceProvider
{


    public function boot()
    {


        //registering commands
        $this->commands([

            CreateRootUser::class,
            PromoteSU::class,
            RemoveRootUser::class,
            RevokeSU::class,
            CreateExampleRootUser::class

        ]);


        //migration file
        $this->publishes([

            __DIR__."/Migrations/2018_11_21_072945_add_is_root_column_to_users_table.php" => database_path("migrations/2018_11_21_072945_add_is_root_column_to_users_table.php")

        ],"azizyus/laravel-su-publish-migration");



        //config file
        $this->publishes([

            __DIR__."/Config/LaravelSuConfig.php" => config_path("LaravelSuConfig.php"),

        ],"azizyus/laravel-su-publish-config");

    }

}