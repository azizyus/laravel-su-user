<?php

namespace LaravelSU\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class RevokeSU extends RootCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'root:revoke';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $userModel = $this->userModel;
        $email = $this->ask("get email to revoke su");

        $user = $userModel::where("email",$email)->first();

        if($user)
        {

            $user->su = false;
            $user->save();
            $this->comment("revoked");

        }
        else $this->comment("cant find user");


    }
}
