<?php

namespace LaravelSU\Console\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateRootUser extends RootCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'root:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userModel = $this->userModel;
        while(true)
        {
            $email = $this->output->ask("Email (be sure its unique): ");
            if($userModel::where("email",$email)->first()) $this->output->warning("This Email Address Already Exist");
            else break;
        }

        $name = $this->output->ask("Name: ");

        while (true)
        {
            $password = $this->output->ask("Password: ");
            $passwordAgain = $this->output->ask("Password Confirm: ");

            if($password === $passwordAgain) break;
            else $this->output->warning("passwords doesnt match");
        }



        $user =  new $userModel();
        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->su = true;
        $user->save();

        $this->output->text("Your ROOT account created");

    }
}
