<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/12/19
 * Time: 1:26 PM
 */

namespace LaravelSU\Console\Commands;


use Illuminate\Console\Command;

abstract class RootCommand extends Command
{

    public $userModel;

    public function __construct()
    {
        parent::__construct();
        $this->userModel = config("LaravelSuConfig.modelNamespace");
    }

}