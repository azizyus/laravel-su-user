<?php

namespace LaravelSU\Console\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateExampleRootUser extends RootCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'root:add-example  {--email=test@mail.com}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userModel = $this->userModel;

        $user =  new $userModel();
        $user->name = "test";
        $user->email = $this->option('email');
        $user->password = Hash::make("123");
        $user->su = true;
        $user->save();

        $this->output->text("Your ROOT EXAMPLE account created (ID: test@mail.com PASS: 123)");

    }
}
