<?php

namespace LaravelSU\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class RemoveRootUser extends RootCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'root:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->ask("Write the email address of account you would like to delete");
        $userModel = $this->userModel;

            if($userModel::where("email",$email)->first())
            {
                if($this->output->confirm("Sure?"))
                {
                    $userModel::where("email",$email)->delete();
                    $this->output->text("You deleted the $email root account");
                }
                else
                {
                    $this->output->text("Nothing deleted");
                }


            }
            else $this->output->text("There is no an user like that email");


    }
}
